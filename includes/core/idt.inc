;---------------------------------------------------------------------------------------------------
; File name:        gdt.inc
; Version:          1.0.0.0
; Author:           Maxime Jeanson
; Copyright:        (c) 2010 by Sep System, all rights reserved.
;
; Documentations:   file:///usr/doc/system/kernel/gdt/index.html
;
; Contains:         This file contain the deffinitions of the fonctions in the gdt.asm file.
;
;                   These fonctions are in this file:
;                    - __gdt.init__:
;                    - __gdt.add.desc__:
;---------------------------------------------------------------------------------------------------

	; MARK: fonction definitions
		%define idt_init call __init_idt__
		
		
