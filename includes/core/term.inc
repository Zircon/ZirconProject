
%ifndef KERNEL_CORE_TERM.INC
%define KERNEL_CORE_TERM.INC

	%define TERM_DATA_MODE_TEXT_ONLY    0x00
	%define TERM_DATA_MODE_COLORED_TEXT 0x01

	struc TERM
	{
		.utid        resb 8
		.data_seg    resw 1
		.data_offset resd 1
		.width       resd 1
		.height      resd 1
		.cursor_x    resd 1
		.cursor_y    resd 1
	}
	endstruc
	
%endif