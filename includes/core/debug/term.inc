;---------------------------------------------------------------------------------------------------
; core/debug/term.inc
;
; Macro and defines of term.asm.
;---------------------------------------------------------------------------------------------------

%ifndef __DEBUG_TERM_INC__
%define __DEBUG_TERM_INC__

    %define null 0x00
	%define bel  0x07
	%define bs	 0x08
    %define tab  0x09
    %define lf   0x0a
    %define vt   0x0b
	%define ff   0x0c
    %define cr   0x0d
	%define esc  0x0e

    %define printf(text) printf text
	%define printh(val, pos) printh val, pos

	%define attr(val) mov byte [attr], val

    ;===============================================================================================

    %macro printf 1

		pushad

        mov ESI, %1

        call __printf__

		popad

    %endmacro

	;-----------------------------------------------------------------------------------------------
	
	%macro printh 2
		
		pushad
				
		mov eax, %1
		mov ecx, %2
		
		call __printh__
		
		popad
		
	%endmacro
	
    ;===============================================================================================

[SECTION .data]
    cursor: dd 0x000B8000 ; Current cursor position.
    attr:   db 0x07       ; Current character attributes.
[SECTION .text]

%endif ; __DEBUG_TERM_INC__
