%ifndef __DEBUG_DISK_INC__
%define __DEBUG_DISK_INC__

	%define disk_access 0x00000000
		%define disk_access.sectCount  (disk_access + 0x00000000)
		%define disk_access.sectNumber (disk_access + 0x00000002)
		%define disk_access.buffer     (disk_access + 0x00000008)
		%define disk_access.drive      (disk_access + 0x0000000c)
		%define disk_access.option     (disk_access + 0x0000000d)

	%macro m_disk_read 1
		%define disk_access %1
		
		call __disk_read__
	%endmacro

	%define disk_read(da_pointer) m_disk_read da_pointer


	__DISK_IDE__:
		.disk0: dw 0x01f0
		.disk1: dw 0x0170

%endif ; __DEBUG_DISK_INC__
