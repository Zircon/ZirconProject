%ifndef __STDDEF__
%define __STDDEF__

	%macro uint8 1
		[SECTION .bss]
		%1: resb 1
		[SECTION .text]
	%endmacro

	%macro uint16 1
		[SECTION .bss]
		%1: resb 2
		[SECTION .text]
	%endmacro

	%macro uint32 1
		[SECTION .bss]
		%1: resb 4
		[SECTION .text]
	%endmacro

	%macro uint64 1
		[SECTION .bss]
		%1: resb 8
		[SECTION .text]
	%endmacro

	%macro uint8 2
		[SECTION .data]
		%1: db %2
		[SECTION .text]
	%endmacro

	%macro uint16 2
		[SECTION .data]
		%1: dw %2
		[SECTION .text]
	%endmacro

	%macro uint32 2
		[SECTION .data]
		%1: dd %2
		[SECTION .text]
	%endmacro

	%macro uint64 2
		[SECTION .data]
		%1: dq %2
		[SECTION .text]
	%endmacro

	%macro uint8_ptr 2
		[SECTION .bss]
		ABSOLUTE %2
		%1: resb 1
		[SECTION .text]
	%endmacro

	%macro uint16_ptr 2
		[SECTION .bss]
		ABSOLUTE %2
		%1: resb 2
		[SECTION .text]
	%endmacro

	%macro uint32_ptr 2
		[SECTION .bss]
		ABSOLUTE %2
		%1: resb 4
		[SECTION .text]
	%endmacro

	%macro uint64_ptr 2
		[SECTION .bss]
		ABSOLUTE %2
		%1: resb 8
		[SECTION .text]
	%endmacro

	%macro void_ptr 3
		[SECTION .bss]
		ABSOLUTE %2
		%1: resb %3
		[SECTION .text]
	%endmacro

	%macro str 2
		[SECTION .data]
		%1: db %2
		[SECTION .text]
	%endmacro

%endif ; __STDDEF__
