;===============================================================================
; kernel.asm
; This file is a part of zircon kernel
;
; Copyright 2011 - Maxime Jeanson
; zircon kernel is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 2 of the License, or
; (at your option) any later version.
;
; zircon kernel is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with zircon kernel; if not, write to the Free Software
; Foundation, Inc., 51 Franklin St, Fifth Floor,
; Boston, MA  02110-1301  USA
;===============================================================================

[ORG 0x100000]
[BITS 32]

%define MULTIBOOT_HEADER_MAGIC  0x1BADB002
%define MULTIBOOT_HEADER_FLAGS  0x00010003
%define CHECKSUM -(MULTIBOOT_HEADER_MAGIC + MULTIBOOT_HEADER_FLAGS)

%define MULTIBOOT_HEADER_GRAPHICS_MODE   0x00000000
%define MULTIBOOT_HEADER_GRAPHICS_WIDTH  80
%define MULTIBOOT_HEADER_GRAPHICS_HEIGHT 50
%define MULTIBOOT_HEADER_GRAPHICS_DEPTH  0x00000020

%define MULTIBOOT_MAGIC 0x2BADB002

start:
	jmp main

align 4
multiboot_header:
dd MULTIBOOT_HEADER_MAGIC
dd MULTIBOOT_HEADER_FLAGS
dd CHECKSUM
dd multiboot_header
dd start
dd 0x00
dd 0x00
dd main

%include "stddef.asm"
%include "core/debug/term.asm"
%include "core/a20.asm"
%include "core/term.inc"
%include "core/stdio.asm"
%include "core/cpu_utils.asm"
%include "core/gdt.inc"
%include "core/gdt.asm"
%include "core/int.asm"
%include "core/idt.inc"
%include "core/idt.asm"
%include "cpu_check.asm"
%include "mm.asm"
%include "multiboot.asm"
%include "pg.asm"
%include "kterm.asm"

uint32 __kintegrity__, __kintegrytyptr__

main:
	mov [multiboot_ptr], ebx

	cmp eax, MULTIBOOT_MAGIC
	jz .ok

	cmt
	
	jhfidlfhdl
	fkdjlfkjd
	fjdlfjld
	dfkjldkjf
	
	cmtend
	
	
	{
		kprintf(kterm_fatal, __SYS_FATAL__)
		CPU_UTILS_HALT
	}

	.ok:
	{
		kprintf(0x70, __SYS_LOADING__)
	}

	cli

	SET_A20_GATE_ON

	printf( __SYS_GDT_INIT__ )

	mov ebx, [multiboot_ptr]
	printh (ebx, 0x04)

	mov eax, [ebx + multiboot_struc.flags]
	printh(eax, 0x04)

	mov eax, [ebx + multiboot_struc.boot_device]
	printh(eax, 0x04)

	mov eax, [ebx + multiboot_struc.mem_upper]
	add eax, 0x440
	mov [PMSize], eax
	printh(dword [PMSize], 0x04)

	mov eax, [ebx + multiboot_struc.mem_lower]
	printh(eax, 0x04)


	gdt.init

	gdt.add.desc ( desc_cs, 1, 0x00000000, 0xFFFFF, 0xd9b )
	gdt.add.desc ( desc_ds, 2, 0x00000000, 0xFFFFF, 0xD93 )
	gdt.add.desc ( desc_ss, 3, 0x00200000, 0x000ff, 0xD93 )
	gdt.add.desc ( gdt,     4, 0x00000000, 0x00800, 0x493 )
	gdt.add.desc ( idt,     5, 0x00000800, 0x00800, 0x493 )
	gdt.add.desc ( video,   6, 0x000B8000, 0x00fa0, 0x493 )
	gdt.add.desc ( ucode,   7, 0x00000000, 0x000ff, 0xdfb )
	gdt.add.desc ( udata,   8, 0x00000000, 0x000ff, 0xdf3 )
	gdt.add.desc ( ustack,  9, 0x00300000, 0x000ff, 0xdf3 )
	gdt.add.desc ( dtss,   10, def_tss,    0x00067, 0x0e9 )

	lgdt [ __GDTR_LIMITE__ ]

	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	mov ax, 0x18
	mov ss, ax

	mov esp, 0x00100000

	jmp 0x08:gdt.init_end

gdt.init_end:

	mov eax, [PMSize]
	printh(eax, 0x04)
	printf(PMSize_str)
	str PMSize_str, {"kio of physical memory", cr, 0x00}
	
	;--------------------------------------------------------------------------;
	
	mov eax, [PMSize]
	shr eax, 0x02
	printh(eax, 0x04)
	printf(PageCount)
	str PageCount, {"Pages Available on this system", cr, 0x00}

	call PMM_INIT

printf(__SYS_IDT_INIT__)

	call __init_idt__

	mov eax, 0x00000008
	mov ebx, irq_00
	mov ecx, 0x00008e00
	mov edx, 0x00000900
	call __idt.add_desc

	mov eax, 0x00000008
	mov ebx, irq_01
	mov ecx, 0x00008e00
	mov edx, 0x00000908
	call __idt.add_desc

	mov eax, 0x00000008
	mov ebx, exc.00
	mov ecx, 0x00008e00
	mov edx, 0x00000800
	call __idt.add_desc

	lidt [__idtr__]

	_pic_config_:
		ICW:
		    ICW_1:
		        mov AL, 0x11
		        out 0x20, AL
		        out 0xA0, AL
		    jmp ICW_2

		    ICW_2:
		        mov AL, 0x20
		        out 0x21, AL
		        mov AL, 0x28
		        out 0xA1, AL
		    jmp ICW_3

		    ICW_3:
		        mov AL, 0x04
		        out 0x21, AL
		        mov AL, 0x02
		        out 0xA1, AL
		    jmp ICW_4

		    ICW_4:
		        mov AL, 0x01
		        out 0x21, AL
		        out 0xA1, AL
		    jmp ICW_FIN

		ICW_FIN:

		OCW:
		    OCW_1:
		        mov AL, 0x00
		        out 0x21, AL
				mov al, 0x00
		        out 0xA1, AL
		    jmp OCW_FIN

		OCW_FIN:

	PIT:
		mov dx, 1193180/100

		mov	al, 00110110b
		out	0x43, al

		mov	ax, dx
		out	0x40, al	;LSB
		xchg ah, al
		out	0x40, al	;MSB


	cli

	call cpu_check

;	kprintf(kterm_good, __SYS_SUCCESS__)
;	kprintf(kterm_warning, __SYS_SUCCESS__)
;	kprintf(kterm_fatal, __SYS_SUCCESS__)
;	kprintf(kterm_normal, __SYS_SUCCESS__)
;	kprintf(kterm_error, __SYS_SUCCESS__)

ata_disk_identify:
{
	push ebp
	mov ebp, esp
	
	mov dx, 0x01f6
	mov al, 0xa0
	out dx, al
	
	mov dx, 0x01f2
	mov al, 0x00
	out dx, al
	
	mov dx, 0x01f3
	mov al, 0x00
	out dx, al
	
	mov dx, 0x01f4
	mov al, 0x00
	out dx, al
	
	mov dx, 0x01f5
	mov al, 0x00
	out dx, al
	
	;-------------------------------------;
	; send command byte
	;-------------------------------------;
	
	mov dx, 0x01f7
	mov al, 0xec
	out dx, al
	
	mov dx, 0x01f7
	in al, dx
	
	.if0:
	{
		cmp al, 0x00
		jne .else0
		
		kprintf(kterm_error, .error_np)
		jmp .ret
	}
	.else0:
	{
		call palloc
		push eax
		mov ecx, eax
		mov dx, 0x01f0
		xor ebx, ebx
		
		.loop0:
		{
			in ax, dx
			xchg al, ah
			mov [ecx], ax
			add ecx, 0x02
			inc ebx
			
			cmp ebx, 256
			jbe .loop0
		}
		
		kprintf(kterm_good, .drive_p)
;		jmp .ret
	}
	
	.ret:
	{
		pop eax
		add eax, 54
		kprintf(kterm_normal, eax)
		
		pop ebp
		ret
	}
	
	str .error_np,	{"This drive is not present!", cr, 0x00}
	str .drive_p,	{"This drive is present!", cr, 0x00}
}

;	pg:
;		mov eax, 0x00400003
;		mov [0x00001000], eax

;		mov eax, 0x00000000
;		mov ebx, 0x00001004

;		.lopd:
;			mov [ebx], eax

;			add ebx, 0x00000004
;			cmp ebx, 0x00002000
;			jb .lopd

;		mov eax, 0x00000003
;		mov ebx, 0x00400000

;		.lop:
;			mov [ebx], eax

;			add eax, 0x00001000
;			add ebx, 0x00000004
;			cmp eax, 0x00400000
;			jb .lop

call mkpd

;		mov eax, 0x00001000
;		mov cr3, eax

;		and eax, 0x00000000
;		mov cr4, eax

;		mov eax, cr0
;		or eax, 0x80000000
;		mov cr0, eax

call kalloc_init

;	mov dx, 0x0cf8
;	mov eax, 0x80000000
;	out dx, eax
;	
;	mov dx, 0x0cfc
;	in ax, dx
;	printh(eax, 0x02)

;	mov dx, 0x0cf8
;	mov eax, 0x80000008
;	out dx, eax
;	
;	mov dx, 0x0cfc
;	in ax, dx
;	printh(eax, 0x02)

sti


	
	
	
	
; 0x02000 to 0x10000 bitmap


;	in	al, 0x21		; read in the primary PIC Interrupt Mask Register (IMR)
;	and	al, 0x00		; 0xEF => 11101111b. This sets the IRQ4 bit (Bit 5) in AL
;	out	0x21, al		; write the value back into IMR
;
;	mov al, 0xf2
;	out 0x60, al
;
;	read_kbrd:
;	in al, 0x64
;	and al, 0x01
;	cmp al, 0x01
;	jne read_kbrd_end
;
;	in al, 0x60
;	mov [cursor], al
;	inc dword [cursor]
;	inc dword [cursor]
;
;	jmp read_kbrd
;
;	read_kbrd_end:


	push __SYS_TERM__
	push __SYS_SUCCESS__
	call printfe

	mov [__SYS_TERM__.cursor_y], dword 0x02

	push __SYS_TERM__
	push __SYS_SUCCESS__
	call printfe

;mov eax, [task1]
;mov [0x00400000], eax
;mov eax, [task1+4]
;mov [0x00400004], eax
;mov eax, [task1+8]
;mov [0x00400008], eax

;	mov ax, 0x43
;	mov es, ax
;	mov ax, 0x08
;	mov ds, ax

;	mov edi, 0x00000000
;	mov esi, task1
;
;	movsd
;	movsd

	mov eax, cr3
	mov [def_tss.cr3], eax
	mov eax, 0x00100000
	mov [def_tss.esp], eax
	mov eax, 0x00100000
	mov [def_tss.esp], eax
	mov eax, 0x00100000
	mov [def_tss.esp], eax
	mov eax, 0x00100000
	mov [def_tss.esp], eax
	mov eax, 0x00100000
	mov [def_tss.esp], eax

	mov ax, 0x53
;	ltr ax






printf(__SYS_SUCCESS__)

;	push word  0x4b
;	push dword esp
;	pushfd
;	pop eax
;	or eax, 0x200
;	and eax, 0xffffbfff
;	push eax
;	push 0x3b
;	push dword task0
;	mov [def_tss.esp0], dword 0x00100000
;	mov ax, 0x43
;	mov ds, ax
	sti
;	iretd

task0:
;mov ax, 0x43
;mov ss, ax

;mov ax, 0x3a
;mov ds, ax

;mov eax, 0x00100000
;mov esp, eax
;sti
;jmp 0x4a:0x00



printf(__SYS_SUCCESS__)

;			mov dx, 0x1ce
;			mov ax, 4
;			out dx, ax
;
;			mov dx, 0x1cf
;			mov ax, 0x00
;			out dx, ax
;
;			mov dx, 0x1ce
;			mov ax, 1
;			out dx, ax
;
;			mov dx, 0x1cf
;			mov ax, 32
;			out dx, ax
;
;			mov dx, 0x1ce
;			mov ax, 2
;			out dx, ax
;
;			mov dx, 0x1cf
;			mov ax, 32
;			out dx, ax
;
;			mov dx, 0x1ce
;			mov ax, 3
;			out dx, ax
;
;			mov dx, 0x1cf
;			mov ax, 0x8
;			out dx, ax
;
;			mov dx, 0x1ce
;			mov ax, 4
;			out dx, ax
;
;			mov dx, 0x1cf
;			mov ax, 0x41
;			out dx, ax
;
;	xor ebx, ebx
;	mov ebx, 0xffffffff
;	mov eax, 0xE00000
;
;	lop:
;		mov [eax], ebx
;		inc eax
;
;		jmp lop

; 	call __scroll_up__

; call disk.read

	CPU_UTILS_HALT

str	__SYS_LOADING__,	{"                                  Zircon System                                 ", 0x00}
str	__SYS_HALT__,		{"The system will be in stand mode.", cr, 0x00}
str	__SYS_FATAL__,		{"The system had encountered a fatal error in the system.", cr, 0x00}
str	__SYS_GDT_INIT__,	{"Initialization of the gdt...", cr, 0x00}
str	__SYS_IDT_INIT__,	{"Initialization of the idt...", cr, 0x00}
str	__SYS_SUCCESS__,	{"[SUCCEED]", 0x00}
str	__SYS_FAILED__,		{" [FAILED]", 0x00}
str	__SYS_WARNING__,	{" [WARNED]", 0x00}

	disk.read:
		mov ebp, esp

		mov dx, 0x01f1
		mov al, 0x00
		out dx, al

		inc dx
		mov al, [ebp + 0x08]
		out dx, al

		inc dx
		mov al, [ebp + 0x10]
		out dx, al

		inc dx
		mov al, [ebp + 0x11]
		out dx, al

		inc dx
		mov al, [ebp + 0x12]
		out dx, al

		inc dx
		mov al, [ebp + 0x13]
		and al, 0xf0
		out dx, al

		inc dx
		mov al, 0x30
		out dx, al

		.ready:
			in al, dx
			and al, 0x08

			jne .ready

		mov esi, [ebp + 14]
		mov dx, 0x01f0
		mov cx, 256

		rep outsw

		ret

	lbl1:
		ret

uint32 multiboot_ptr

__SYS_TERM__:
{
	.utid:        db "SYS_TERM"
	.data_seg:    dw 0x0030
	.data_offset: dd 0x00000000
	.width:       dd 160
	.height:      dd 25
	.cursor_x:    dd 0x000000a0
	.cursor_y:    dd 0x01
}

__term:
{

}

__scroll_up__:
{
	mov ax, video
	mov ds, ax
	mov es, ax

	mov eax, 0x00000000
	mov esi, eax

	mov eax, 0x000000a0
	mov edi, eax

	mov ecx, 0x00000f00
	rep movsb

	ret
}

[SECTION .data]
def_tss:
{
	.previous_task:			dw 0x0000
	.previous_task_unused:	dw 0x0000
	.esp0:					dd 0x00100000
	.ss0:					dw 0x0018
	.ss0_unused:			dw 0x0000
	.esp1:					dd 0x00000000
	.ss1:					dw 0x0000
	.ss1_unused:			dw 0x0000
	.esp2:					dd 0x00000000
	.ss2:					dw 0x0000
	.ss2_unused:			dw 0x0000
	.cr3:					dd 0x00000000
	.eip:					dd 0x00000000
	.eflags:				dd 0x00000000
	.eax:					dd 0x00000000
	.ecx:					dd 0x00000000
	.edx:					dd 0x00000000
	.ebx:					dd 0x00000000
	.esp:					dd 0x00000000
	.ebp:					dd 0x00000000
	.esi:					dd 0x00000000
	.edi:					dd 0x00000000
	.es:					dw 0x0000
	.es_unused:				dw 0x0000
	.cs:					dw 0x003b
	.cs_unused:				dw 0x0000
	.ss:					dw 0x004b
	.ss_unused:				dw 0x0000
	.ds:					dw 0x0043
	.ds_unused:				dw 0x0000
	.fs:					dw 0x0043
	.fs_unused:				dw 0x0000
	.gs:					dw 0x0043
	.gs_unused:				dw 0x0000
	.ldt_selector:			dw 0x0000
	.ldt_sel_unused:		dw 0x0000
	.debug_flag:			dw 0x0000
	.io_map:				dw 0x0000
}
[SECTION .text]

task1:
{
	xor eax, eax
	jmp eax
}

PMSize: dd 0x00000000

__kintegrytyptr__:
