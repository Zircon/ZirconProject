%ifndef __KTERM_ASM__
%define __KTERM_ASM__

	%include "kterm.inc"

	kprintf:
	{
		push ebp
		mov ebp, esp
		mov esi, [ebp + 0x08]

		.checking:
		{
			; Initialization
			mov al, byte [ esi ]
			inc esi

			; Checking
			cmp al, null
			je .ret

			cmp al, tab
			je .tab

			cmp al, lf
			je .lf

			cmp al, vt
			je .vt

			cmp al, cr
			je .cr
		}

		;-----------------------------------------------------------------------

		.char:
		{
			mov ebx, [cursor]
			mov ah,  [ebp + 0x0c]

			mov word [ebx], ax
			add ebx, 0x02

			mov [cursor], ebx

			jmp .checking
		}

		;-----------------------------------------------------------------------

		.tab:
		{
			add [cursor], dword 0x00000008

			jmp .checking
		}

		;-----------------------------------------------------------------------

		.lf:
		{
			xor edx, edx
			mov eax, [cursor]

			sub eax, 0x000b8000

			mov ebx, 0x000000a0

			div bx
			mul bx

			add eax, 0x000b8000

			mov [cursor], eax

			jmp .checking
		}

		;-----------------------------------------------------------------------

		.vt:
		{
			add [cursor], dword 0x000000A0

			jmp .checking
		}

		;-----------------------------------------------------------------------

		.cr:
		{
			xor edx, edx
			mov eax, [cursor]

			sub eax, 0x000b8000

			mov ebx, 0x000000a0

			div ebx
			inc eax
			mul ebx

			add eax, 0x000b8000

			mov [cursor], eax

			jmp .checking
		}

		;-----------------------------------------------------------------------

		.ret:
		{
			pop ebp
			jmp kterm.setCur
		}
	}

	;---------------------------------------------------------------------------

	kterm.setCur:
	{
		mov ebx, [cursor]
		
		xor edx, edx
		
		sub ebx, 0x000b8000
		shr bx, 1
		
		mov dx, 0x03D4	; Affecte la valeur 0x03D4 au registre dx.
		mov al, 0x0E	; Affecte la valeur 0x0E au registre al.
		out dx, al		; Envoie de al au port de sortie dx.
			
		mov dx, 0x03D5	; Affecte la valeur 0x03D5 au registre dx.
		mov al, bh		; Affecte le registre BH au registre al.
		out dx, al		; Envoie de al au port de sortie dx.

		mov dx, 0x03D4	; Affecte la valeur 0x03D4 au registre dx.
		mov al, 0x0F	; Affecte la valeur 0x0F au registre al.
		out dx, al		; Envoie de al au port de sortie dx.

		mov dx, 0x03D5	; Affecte la valeur 0x03D5 au registre dx.
		mov al, bl		; Affecte le registre BL au registre al.
		out dx, al		; Envoie de al au port de sortie dx.

		ret
	}

%endif ; __KTERM_ASM__
