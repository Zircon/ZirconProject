[BITS 32]

%define WIDTH  4
%define HEIGHT 4
%define BITSPERPIXEL 32
%define BITMAPSIZE ((BITSPERPIXEL/8)*(WIDTH)*(HEIGHT))

Header:
	db 'BM'
	dd end
	dd 0x00
	dd start
	dd 40
	dd WIDTH
	dd HEIGHT
	dw 1
	dw BITSPERPIXEL
	dd 0
	dd BITMAPSIZE
	dd 70
	dd 70
	dd (2^24)
	dd (2^24)
	; arvb
start:
	dd 0xffff0000, 0x66998836, 0x44ffffff, 0xff0000cc
	dd 0xff00ff00, 0x66998836, 0x44ffffff, 0xff0000cc
	dd 0xff0000ff, 0x66998836, 0x44ffffff, 0xff0000cc
	dd 0xff000000, 0x66998836, 0x44ffffff, 0xff0000cc
end: