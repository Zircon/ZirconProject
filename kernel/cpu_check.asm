;===============================================================================
; This file contain the original and/or modified code. It comes with no waranty 
; and could be modified.
;
; cpu_check.asm
; This file is a part of zircon kernel
;
; Copyright 2011 - Maxime Jeanson
; zircon kernel is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 2 of the License, or
; (at your option) any later version.
;
; zircon kernel is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with zircon kernel; if not, write to the Free Software
; Foundation, Inc., 51 Franklin St, Fifth Floor, 
; Boston, MA  02110-1301  USA
;===============================================================================

%ifndef cpu_check.asm
%define cpu_check.asm

	cpu_vendor_id:
		str	.intel,	"GenuineIntel"
		str	.amd,	"AuthenticAMD"

	cpu_model_id:
		.family:	dw 0x0000
		.model:		db 0x00
		.stepping:	db 0x00

	cpu_feature_flags:
		.edx: dd 0x00000000
		.ecx: dd 0x00000000

	cpu_misc_info: dd 0x00000000

	cpu_check:
		call cpu_check_amd
		call cpu_check_intel

		end:
			ret

		cpu_check_intel:
			.isGenuineIntel:
				xor eax, eax
				xor ebx, ebx
				xor ecx, ecx
				xor edx, edx
			
				cpuid

				push eax
				mov ebp, esp

				cmp ebx, [cpu_vendor_id.intel]
				jne .notIntel

				cmp edx, [cpu_vendor_id.intel + 0x04]
				jne .notIntel

				cmp ecx, [cpu_vendor_id.intel + 0x08]
				jne .notIntel

			.getIdSignature:
				mov eax, 0x00000001
				cmp eax, [ebp]
				jb .end

				cpuid

				push ebx
				push ecx
				push edx

				mov ebx, eax
				and al, 0x0f
				mov [cpu_model_id.stepping], al

				mov ecx, ebx
				and cl, 0xf0
				shr cl, 0x04
				mov eax, ebx
				and eax, 0x000f0000
				shr eax, 0x0c
				add cl, al
				mov [cpu_model_id.model], cl

				mov ecx, ebx
				and cx, 0x0f00
				shr cx, 0x08
				mov eax, ebx
				and eax, 0x0ff00000
				shr eax, 0x10
				add cx, ax
				mov [cpu_model_id.family], cx

			.getFeatureFlags:
				pop eax
				mov [cpu_feature_flags.edx], eax
				pop eax
				mov [cpu_feature_flags.ecx], eax

			.getMiscInfo:
				pop eax
				mov [cpu_misc_info], eax

			.getPSN:
				mov eax, [cpu_feature_flags]

			.notIntel:

			.end:
				pop eax
				ret


		cpu_check_amd:
			.isAuthenticAMD:
				xor eax, eax
				xor ebx, ebx
				xor ecx, ecx
				xor edx, edx

				cpuid

				push eax
				mov ebp, esp

				cmp ebx, [cpu_vendor_id.amd]
				jne .notAmd

				cmp edx, [cpu_vendor_id.amd + 0x04]
				jne .notAmd

				cmp ecx, [cpu_vendor_id.amd + 0x08]
				jne .notAmd

			.getIdSignature:
				mov eax, 0x00000001
				cmp eax, [ebp]
				jb .end

				cpuid

				push ebx
				push ecx
				push edx

				mov ebx, eax
				and al, 0x0f
				mov [cpu_model_id.stepping], al

				mov ecx, ebx
				and cl, 0xf0
				shr cl, 0x04
				mov eax, ebx
				and eax, 0x000f0000
				shr eax, 0x0c
				add cl, al
				mov [cpu_model_id.model], cl

				mov ecx, ebx
				and cx, 0x0f00
				shr cx, 0x08
				mov eax, ebx
				and eax, 0x0ff00000
				shr eax, 0x10
				add cx, ax
				mov [cpu_model_id.family], cx

			.getFeatureFlags:
				pop eax
				mov [cpu_feature_flags.edx], eax
				pop eax
				mov [cpu_feature_flags.ecx], eax

			.getMiscInfo:
				pop eax
				mov [cpu_misc_info], eax

			.getPSN:
				mov eax, [cpu_feature_flags]

			.notAmd:

			.end:
				pop eax
				ret

%endif ; cpu_check.asm

