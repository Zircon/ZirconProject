;===============================================================================
; a20.asm
;
; a20 gate configuration file. It allow you to enable and disable the a20 gate
; to access all 32 bits memory. That file is a part of the zircon kernel.
;===============================================================================
%ifndef __A20_ASM__
%define __A20_ASM__

	%define SET_A20_GATE_ON   call a20_gate.on
	%define SET_A20_GATE_OFF  call a20_gate.off

	a20_gate:
	{
		.on:
		{
			xor al, al

			in al, 0x92
			or al, 0x02
			out 0x92, al

			ret
		}

		;-----------------------------------------------------------------------
		.off:
		{
			xor al, al

			in al, 0x92
			and al, 0xfd
			out 0x92, al

			ret
		}
	}

%endif ; __A20_ASM__
