[BITS 32]
[ORG 0x1000]

jmp main 

%include "core/debug/term.asm"
%include "core/debug/disk.asm"

__SYS_LOADING__: db '                                 Feather System                                 ', 0x00
__SYS_INIT__:    db ' - System initialisation', cr, 0x00
__SYS_ERROR__:   db ' - System error : file not found.', cr, 0x00
tmp: dd '  ', 0x0000
disk:
	.sectCount:  db 0x03
	.sectNumber: db 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	.buffer:     dd 0x000b8000
	.drive:      db 0x00
	.option:     db 0x24


main:
	attr (0x70)
	printf (__SYS_LOADING__)
	attr (0x02)
	printf (__SYS_INIT__)
	attr (0x04)
	printf (__SYS_ERROR__)
	attr (0x07)

;	call __disk_check__

;		mov dx, 0x1ce
;		mov ax, 4
;		out dx, ax
;		
;		mov dx, 0x1cf
;		mov ax, 0x00
;		out dx, ax
;		
;		mov dx, 0x1ce
;		mov ax, 1
;		out dx, ax
;		
;		mov dx, 0x1cf
;		mov ax, 512
;		out dx, ax
;		
;		mov dx, 0x1ce
;		mov ax, 2
;		out dx, ax
;		
;		mov dx, 0x1cf
;		mov ax, 512
;		out dx, ax
;
;		mov dx, 0x1ce
;		mov ax, 3
;		out dx, ax
;		
;		mov dx, 0x1cf
;		mov ax, 0x4
;		out dx, ax
;		
;		mov dx, 0x1ce
;		mov ax, 4
;		out dx, ax
;		
;		mov dx, 0x1cf
;		mov ax, 0x81
;		out dx, ax
;		
;		mov dx, 0x1ce
;		mov ax, 5
;;		out dx, ax
;;		
;;		mov dx, 0x1cf
;;		mov ax, 0x81
;		out dx, ax
;;	
	
;	call __ide_check__
	
			
			mov dx, 0x1f1
			mov al, 0x00
			out dx, al

			mov dx, 0x1f2
			mov al, 0x02
			out dx, al
		
			mov dx, 0x1f3
			mov al, 0x00
			out dx, al
		
			mov dx, 0x1f4
			mov al, 0x00
			out dx, al
		
			mov dx, 0x1f5
			mov al, 0x00
			out dx, al
		
			mov dx, 0x1f6
			mov al, 0xe0
			out dx, al
		
			
			mov dx, 0x1f7
			mov al, 0x20
			out dx, al

			xor edx, edx
			xor eax, eax
			
			mov ebx, 3
			
			stand:
				mov dx, 0x01f7
				in al, dx
				and al, 0x08
				cmp al, 0x00

				jz stand

;			stand2:
;				in al, dx
;				and al, 0x08
;				cmp al, 0x08
;				printf (__SYS_LOADING__)

;				jz stand2

			xor ecx, ecx
			
			mov dx, 0x1f0
			__read_data__ :
				in ax, dx
				
				mov [tmp], ax
				printf(tmp)
;				printh(ax, 0x02)

				
				
				add ecx, 2
				
				cmp ecx, 512
				jb __read_data__
				
				dec ebx
				
				cmp ebx, 0
				ja stand
				
;	mov dx, 0xCF8
;	mov eax, 0x80000800
;	out dx, eax
	
;	mov dx, 0xcfc
;	in ax, dx
	
;	mov [0xb8004], ax

;	mov dx, 0xCF8
;	mov eax, 0x80000804
;	out dx, eax
	
;	mov dx, 0xcfc
;	in ax, dx
	
;	mov [0xb8006], ax

;	mov dx, 0xCF8
;	mov eax, 0x80000808
;	out dx, eax
	
;	mov dx, 0xcfc
;	in ax, dx
	
;	mov [0xb8008], ax

	hlt
	jmp main


times 1474048-($-$$) db 0x00
