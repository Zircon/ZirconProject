;===============================================================================
; This file contain the original and/or modified code. It comes with no waranty 
; and could be modified.
;
; int.asm
; This file is a part of zircon kernel
;
; Copyright 2011 - Maxime Jeanson
; zircon kernel is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 2 of the License, or
; (at your option) any later version.
;
; zircon kernel is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with zircon kernel; if not, write to the Free Software
; Foundation, Inc., 51 Franklin St, Fifth Floor, 
; Boston, MA  02110-1301  USA
;===============================================================================

%ifndef int.asm
%define int.asm


	%define KBRD_STAT_LSHIFT	0x01
	%define KBRD_STAT_RSHIFT	0x02
	%define KBRD_STAT_CTRL		0x04
	%define KBRD_STAT_ALT		0x08
	%define KBRD_STAT_VERRMAJ	0x10
	%define KBRD_STAT_HOME		0x20
	%define KBRD_STAT_ALTGR		0x40
	%define KBRD_STAT_VERRNUM	0x80


	int_std:
	{
		printf(.error_msg)

		mov al, 0x20
		out 0x20, al

		iretd

		str	.error_msg,	{"Warning : This interrupt is unable or undefined", cr, 0x00}
	}

	exc:
	{
		exc.00:
		{
			
			printf(.warning)

		mov al, 0x20
		out 0x20, al

		iretd


		}
	}
			str	exc.00.warning,	{"Warning : You can't do a divide by zero.", cr, 0x00}

	irq.00.count: db 0x00
	irq.00.sec: db 0x00
	irq.00.min: db 0x00
	irq.00.hrs: db 0x00

	irq_00:
	{

		;AfficherCaractere('C')



		.addCount:
			inc byte [irq.00.count]
			cmp byte [irq.00.count], 100
			jbe .end

		.addSec:
			mov byte [irq.00.count], 0x00
			inc byte [irq.00.sec]
			cmp byte [irq.00.sec], 60
			jb .showTime

		.addMin:
			mov byte [irq.00.sec], 0x00
			inc byte [irq.00.min]
			cmp byte [irq.00.min], 60
			jb .showTime

		.addHrs:
			mov byte [irq.00.min], 0x00
			inc byte [irq.00.hrs]
			cmp byte [irq.00.hrs], 24
			jb .showTime

			mov byte [irq.00.hrs], 0x00

		.showTime:
			mov ecx, 0x01

			mov al, [irq.00.hrs]
			mov [0xb8f9a], al

			mov al, [irq.00.min]
			mov [0xb8f9c], al

			mov al, [irq.00.sec]
			mov [0xb8f9e], al

		.end:

		mov al, 0x20
		out 0x20, al

		iret

	}

	kbrd_map: db esc, '1', '2', '3', '4', '5', '6', '7', '8', '9'
			  db '0', '-', '=', bs , tab, 'q', 'w', 'e', 'r', 't'
			  db 'y', 'u', 'i', 'o', 'p', '^', '�', cr,  0x0, 'a'
			  db 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '�'
			  db '�', 0x0, '�', 'z', 'x', 'c', 'v', 'b', 'n', 'm'
			  db ',', '.', '�', 0x0, '*', 0x0, ' ', 0x0, 0x0, 0x0
			  db 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0
		      db '=', '7', '8', '9', '-', '4', '5', '6', '+', '1'
			  db '2', '3', '0', '.', 0x0, 0x0, 0x0, 0x0, 0x0, 0x0
			  db 0x0, 0x0, '7', '8', '9', '-', '4', '5', '6', '+'
			  db '1', '2', '3', '0', '.', 0x0, 0x0, 0x0, 0x0, 0x0
			  db 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0
			  db 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0

	; f1 = 3a -> f10 = 43

	irq_01:
	{
		uint8 kbrd_stat

		.ready:
		{
			in al, 0x64
			
			and al, 0x01
			je .ready
		}

		in al, 0x60

		dec al

		cmp al, 0x80
		jae .bk_code
		
		.mk_code:
		{
			cmp al, 0x29
			je .mk_code.lshift

			cmp al, 0x35
			je .mk_code.rshift

			cmp al, 0x1c
			je .mk_code.ctrl

			cmp al, 0x37
			je .mk_code.alt

			xor ebx, ebx
			mov bl, al
			add ebx, kbrd_map

			mov al, [ebx]

			mov ecx, 0x01
			call __putc__
			jmp .__ret__
			
			.mk_code.lshift:
			{
				or byte [kbrd_stat], KBRD_STAT_LSHIFT
			}

			.mk_code.rshift:
			{
				or byte [kbrd_stat], KBRD_STAT_RSHIFT
			}

			.mk_code.ctrl:
			{
				or byte [kbrd_stat], KBRD_STAT_CTRL
			}

			.mk_code.alt:
			{
				or byte [kbrd_stat], KBRD_STAT_ALT
			}
		}
		
		.bk_code:
		{
			sub al, 0x80
		
			cmp al, 0x29
			je .bk_code.lshift

			cmp al, 0x35
			je .bk_code.rshift

			cmp al, 0x1c
			je .bk_code.ctrl

			cmp al, 0x37
			je .bk_code.alt
					
			.bk_code.lshift:
			{
				xor byte [kbrd_stat], KBRD_STAT_LSHIFT
			}

			.bk_code.rshift:
			{
				xor byte [kbrd_stat], KBRD_STAT_RSHIFT
			}

			.bk_code.ctrl:
			{
				xor byte [kbrd_stat], KBRD_STAT_CTRL
			}

			.bk_code.alt:
			{
				xor byte [kbrd_stat], KBRD_STAT_ALT
			}
		}

		.__ret__:
		
		mov al, 0x20
		out 0x20, al
		
		iret
	}

%endif ; int.asm

