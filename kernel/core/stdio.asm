
%ifndef KERNEL_CORE_STDIO.ASM
%define KERNEL_CORE_STDIO.ASM
	
	getPMS:
	{
		
	}
	
	printfe:
	{	
		mov edx, [esp + 8]
		mov ax, [edx + 8]
		mov es, ax
		
		mov eax, [edx + 26]
		mov ebx, 160
		mul ebx
		mov ebx, 0x8e
		add eax, ebx
		
		mov edi, eax
		mov esi, [esp + 4]
		
		xor ebx, ebx
		
		lope:
			movsb
			mov byte [es:edi], 0x02
			inc edi
			inc ebx
			
			cmp ebx, 9
			jb lope
		
		ret
	}

%endif

