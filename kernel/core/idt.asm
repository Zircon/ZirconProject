;---------------------------------------------------------------------------------------------------
; File name:        idt.asm
; Version:          1.0.0.0
; Author:           Maxime Jeanson
; Copyright:        (c) 2010 by Sep System, all rights reserved.
;
; Documentations:   file:///usr/doc/system/kernel/gdt/index.html
;
; Contains:         This file contain fonctions to initialize and manage the global descriptor table
;                   of the kernel for the intel 32 bits architecture.
;
;                   These fonctions are in this file:
;                    - __idt.init__:
;                    - __idt.add.desc__:
;---------------------------------------------------------------------------------------------------

%ifndef __IDT_ASM__
%define __IDT_ASM__

	;-----------------------------------------------------------------------------------------------
	; This fonction initialize the Global Descriptors Table Register, it take 0 parameters and need
	; to be run before the other fonctions in this file.
	;-----------------------------------------------------------------------------------------------
    __idtr__ :
		; Initialization
			__idtr_limit__ : dw 0x07FF
			__idtr_base__  : dd 0x00000800
		
		; Return
			ret


	__init_idt__:
		mov edx, dword 0x00000800

		mov eax, dword 0x00000008
		mov ecx, dword 0x00008e00

		.setup:
			mov ebx, dword int_std

			call __idt.add_desc

			cmp edx, 0x00001000
			jb .setup

		ret
	

	
    ;-----------------------------------------------------------------------------------------------
	; This fonction add an descriptor inside the Global Descriptors Table and set it's value.
	; Parameters:
    ;  - eax: Base address 32 bits.
    ;  - ebx: Limit 20 bits.
    ;  - cx:  Attribute 12 bits.
    ;         11  10  9   8   7   6   5   4            0
    ;         +---+---+---+---+---+---+---+------------+
    ;         |   | D |   | A |   | D |   |            |
    ;         | G | / | 0 | V | P | P | S |    type    |
    ;         |   | B |   | L |   | L |   |            |
    ;         +---+---+---+---+---+---+---+------------+
    ;         G:    Indicate if the limit is in byte ( 0 ) or in pages of 4 ko.
    ;         D/B:  Indicate if this descriptor work with 16 bits data ( 0 ) or 32 bits data ( 1 ).
    ;         AVL:  This bits is not use by the processor so you can use it for anything.
    ;         P:    Indicate if this segment is present ( 1 ) or not in the physical memory.
	;         DPL:  Indicate the previlege level of the segment. Root ( 00 ) to users ( 11 ).
    ;         S:    Indicate if it's a system descriptor ( 0 ) or a segment descriptor ( 1 ).
    ;         type: Indicate what is the type of descriptor ( code, data or stack ).
    ;-----------------------------------------------------------------------------------------------
    __idt.add_desc:

        ; Writing
            mov [ edx ], bx ; Write the base 0-15.
            shr ebx, 16     ; Shift right the 32 bits base.
			add edx, 2      ; Add 2 to the pointer.

            mov [ edx ], ax ; Write the segment selector 16-31.
            add edx, 2      ; Add 2 to the pointer.

            mov [ edx ], cx ; Write the attribute 32-47.
            add edx, 2      ; Add 2 to the pointer.
            
            mov [ edx ], bx ; Write the base 48-63.
			add edx, 2

        ; Saving
;            mov [__idtr_limit__], edx
			;add word [__idtr_limit__], 0x8 ; Add one descriptor to the limit of the gdtr.
        
        ; Return
            ret

%endif ; __IDT_ASM__
