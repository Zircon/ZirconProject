;===============================================================================
; cpu_utils.asm
;
; cpu configuration file. It allow you to reset, halt, etc the cpu.
;===============================================================================
%ifndef __CPU_UTILS_ASM__
%define __CPU_UTILS_ASM__

	%define CPU_UTILS_RESET jmp cpu_utils.reset
	%define CPU_UTILS_HALT  jmp cpu_utils.halt

	cpu_utils:
	{
		.reset:
		{
			xor al, al

			in al, 0x92
			or al, 0x01
			out 0x92, al
		}

		;-----------------------------------------------------------------------

		.halt:
		{
			hlt
			CPU_UTILS_HALT
		}
	}

%endif ; __CPU_UTILS_ASM__

