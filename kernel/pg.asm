clearMem:
{
	push ebp
	mov ebp, esp
	mov edx, [ebp + 0x08]
	mov ecx, edx
	add ecx, 0x1000
	
	.loop0:
	{
		mov [edx], dword 0x00
		add edx, 0x04
		
		cmp edx, ecx
		jb .loop0
	}
	
	pop ebp
	ret
}

mkpte:
{
	call palloc
	push eax
	call clearMem
	
	
}

mkpd:
{
	push ebp
	mov ebp, esp
	
	call palloc
	push eax
	call clearMem

	mov ebx, [ebp - 0x04]
	mov eax, ebx
	or eax, 0x03
	
	mov [ebx + 0xffc], eax
	pop eax
	
	pop ebp
	ret
}

setentry:
{
	push ebp
	mov ebp, esp
	mov edx, [ebp + 0x08]
	mov ecx, [ebp + 0x0c]
	or ecx, 0x03
	
	mov [edx], ecx
	
	pop ebp
	ret
}

mkpt:
{
	push ebp
	mov ebp, esp
	
	call palloc
	push eax
	call clearMem
	pop eax
	
	pop ebp
	ret
}
