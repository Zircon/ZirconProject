%ifndef __F_MULTIBOOT_DEF__
%define __F_MULTIBOOT_DEF__

	struc multiboot_struc
	{
		.flags:			resb 4
		.mem_lower:		resb 4
		.mem_upper:		resb 4
		.boot_device:	resb 4
		.cmdline:		resb 4
		.mods_count:	resb 4
		.mods_addr:		resb 4
		.syms:			resb 16
		.mmap_length:	resb 4
		.mmap_addr:		resb 4
	}
	endstruc

%endif ; __F_MULTIBOOT_DEF__
