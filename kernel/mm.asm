PMM_INIT:
{
	mov eax, [PMSize]
	shr eax, 0x02
	
	mov [__PMAT_SIZE__], eax
	
	mov ebx, 0x00300000
	add eax, ebx

	xor ecx, ecx

	.loop:
	{
		mov [ebx], ecx
		
		add ebx, 0x00000004
		cmp ebx, eax
		jbe .loop
	}
	
	mov ebx, 0x00300000
	mov eax, 0x00000002
	add eax, ebx

	.loop0:
	{
		mov [ebx], byte 0x01
		inc ebx
		
		cmp ebx, eax
		jb .loop0
	}
	
	mov ebx, 0x00300028
	mov eax, 0x003000c0
	.loop1:
	{
		mov [ebx], byte 0x01
		inc ebx
		
		cmp ebx, eax
		jb .loop1
	}
	
	mov ebx, 0x00300300
	mov eax, [__PMAT_SIZE__]
	shr eax, 0x0c
	add eax, ebx
	
	.loop2:
	{
		mov [ebx], byte 0x01
		inc ebx
		
		cmp ebx, eax
		jb .loop2
	}

	
	ret
}

uint32	__PMAT_PTR__,	0x00300000
uint32	__PMAT_SIZE__

__PMM_ERROR:
{
	str	.PMF__, {"FATAL: Physical memory is full, the system will crash!", cr, 0x00}
}



palloc:
{
	mov edx, [__PMAT_PTR__]
	mov ecx, [__PMAT_SIZE__]
	
	add ecx, edx
	
	.loop0:
	{
		cmp edx, ecx
		jae .pmf
	
		inc edx
		mov al, [edx]
		and al, 0x01
		cmp al, 0x01
		je .loop0
	}
	
	or bl, 0x01
	mov [edx], bl
	
	sub edx, [__PMAT_PTR__]
	shl edx, 0x0c
	
	mov eax, edx
	
	ret
	
	.pmf:
	{
		printf(__PMM_ERROR.PMF__)
		
		.pmf.loop0:
		{
			jmp .pmf.loop0
		}
	}
}

pfree:
{
	mov ebp, esp
	mov edx, [__PMAT_PTR__]
;	mov eax, [ebp + 0x08]

	shr eax, 0x0c
	add edx, eax

	mov [edx], byte 0x00

	ret
}

;paloc:
{
	mov ebp, esp
	mov eax, [ebp + 0x08]
	
	mov ebx, [__PMAT_PTR__]
	mov edx, [__PMAT_SIZE__]
	add edx, ebx
	.loop:
	{
		cmp ebx, edx
		jae .loop.end
		
		mov cl, [ebx]
		and cl, 0x01
		je .loop
	}
	.loop.end:
	{
		sub ebx, [__PMAT_PTR__]
		
		shl ebx, 0x0c
	}
	
	or ebx, 0x00000007
	mov [eax], ebx
	
	ret
}

struc __kalloc_header_struc
{
	.used:	resb 4
	.free:	resb 4
	.first:	resb 4
	.size:	resb 4
}
endstruc

struc __kalloc_entry_struc
{
	.prev:	resb 2
	.ptr:	resb 4
	.size:	resb 4
	.type:	resb 4
	.next:	resb 2
}
endstruc

kalloc_init:
{
	uint32 __KALLOC_PTR__
	call palloc
	mov [__KALLOC_PTR__], eax
	
	mov [eax + __kalloc_header_struc.first],	dword 0x01
	mov [eax + __kalloc_header_struc.size],		dword 0xffffff00
	mov [eax + __kalloc_header_struc.free],		dword 0xffffff00
	mov [eax + __kalloc_header_struc.used],		dword 0x00000000
	
	add eax, 0x00000008
	mov [eax + __kalloc_entry_struc.prev],	word  0x00
	mov [eax + __kalloc_entry_struc.ptr],	dword 0x00
	mov [eax + __kalloc_entry_struc.size],	dword 0xffffff00
	mov [eax + __kalloc_entry_struc.type],	dword 0x00
	mov [eax + __kalloc_entry_struc.next],	word  0x00
	printh(eax, 0x04)
	ret
}


