%ifndef __KTERM_INC__
%define __KTERM_INC__

	%define kterm_warning	0x0e
	%define kterm_fatal		0x04
	%define kterm_error		0x06
	%define kterm_good		0x02
	%define kterm_normal	0x07

	;--------------------------------------------------------------------------;

	%define kprintf(mode, ptr) kprintf mode, ptr

	;--------------------------------------------------------------------------;

	%macro kprintf 2
	{
		push word	%1
		push dword	%2

		call kprintf

		add esp, 0x06
	}
	%endmacro

%endif ; __KTERM_INC__

