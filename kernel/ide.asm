
ide_common:
{
	uint16 disk_io, { 0x01f0, 0x0170 }

	mov ebp, esp

	xor eax, eax
	mov ax, [ebp + 0x10]
	and ax, 0x02
	add ax, disk_io
	mov dx, [eax]
	push dx

	inc dx
	mov al, 0x00
	out dx, al

	inc dx
	mov ax, [ebp + 0x12]
	out dx, ax

	inc dx
	mov al, [ebp + 0x14]
	mov ah, [ebp + 0x17]
	out dx, ax

	inc dx
	mov al, [ebp + 0x15]
	mov ah, [ebp + 0x18]
	out dx, ax

	inc dx
	mov al, [ebp + 0x16]
	mov ah, [ebp + 0x19]
	out dx, ax

	inc dx
	mov ax, [ebp + 0x10]
	and ax, 0x01
	shl ax, 0x04
	out dx, al

	ret
}

ide_read:
	
