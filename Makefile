ASM=yasm
FORMAT=bin
OBJDIR=$(BUILDDIR)/objects
BUILDDIR=build
FLAGS= -f $(FORMAT) -w -o $(OBJDIR)/$@ -i "includes"

zircon.krnl: kernel.o
	@cat $(OBJDIR)/kernel.o > $@

include ./kernel/*.mkfile

install: feather.krnl
	@sh install
